<?php
class Connect
{
public $config = array(
         "host" => "127.0.0.1", // хост базы данных сервера
         "user" => "root", // пользователь базы данных сервера
         "pass" => "", // пароль от пользователя базы данных сервера
         "port" => "3306", // порт для подключения сервера
         "base" => array(
              "db" => "", // Название бд
          )
      );

function __construct()
  {
    $this->db = new mysqli($this->config['host'], $this->config['user'], $this->config['pass'], $this->config['base']['db'],$this->config['port']);
    if ($this->db->connect_error) {
        die('Error connect base.');
    }
    date_default_timezone_set('Europe/Moscow');
    $this->db->set_charset("utf8");    
  }
  function __destruct()
  {
    $this->db = null;
  }
public function Change_database($name)
  {
   $this->db->select_db($name);
  }
}