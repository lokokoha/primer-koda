<?php
class User
{
    var $connect;
    var $hash;
	function __construct($hash)
	{
	    $this->connect = new Connect();
	    $this->hash = $hash;
	}
    public function GetAccountInfo(){
        return $this->connect->db->query("SELECT login,name,otch,fam FROM `account` WHERE `cache` = '$this->hash'")->fetch_assoc();
    }
    public function UpdateAccountInfo($f,$n,$o) {
        $stmt=$this->connect->db->prepare("UPDATE `account` SET `fam`=?,`name`=?,`otch`=? WHERE `cache`= ?");
        $stmt->bind_param('ssss',$f,$n, $o, $this->hash);
        $stmt->execute();
    }
    public function UpdateAccountPassword($newPassword){
        $accountInfo = $this->GetAccountInfo();
        $login = new Login();
        $newPassword = $login->GenerationCachePassword($accountInfo['login'],$newPassword);
        $stmt=$this->connect->db->prepare("UPDATE `account` SET `password`=? WHERE `cache`= ?");
        $stmt->bind_param('ss',$newPassword,$this->hash);
        $stmt->execute();
    }
}