<?php
    session_start();
    $login = new Login();
    setcookie("cache", "", time()-90600);
    if(isset($_POST['auth'])){
        $login->Authorization($_POST['login'],$_POST['password']);
    }
    if(isset($_POST['reg'])){
        $login->CreateAccount($_POST['login'],$_POST['password'],$_POST['email'],$_POST['f'],$_POST['n'],$_POST['o']);
    }
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Тестовое задание</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="/template/css/style.css">
</head>
<body>
<div class="form">
    <form class="form-horizontal" action method="POST">
        <div class="form-group">
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Логин</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Логин" name="login" required>
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">Пароль</label>
                <div class="col-sm-10">
                    <input type="password" class="form-control" placeholder="Пароль" name="password" required>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" name="auth" class="btn btn-default btn-sm">Войти</button>
                </div>
            </div>
    </form>
    <form class="form-horizontal" action method="POST">
        <div class="form-group">
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Логин</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Логин" name="login" required>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Почта</label>
                <div class="col-sm-10">
                    <input type="email" class="form-control" placeholder="Email" name="email" required>
                </div>
            </div>

            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Фамилия</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Фамилия" name="f" required>
                </div>
            </div>

            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Имя</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Имя" name="n" required>
                </div>
            </div>

            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Отчество</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Отчество" name="o" required>
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">Пароль</label>
                <div class="col-sm-10">
                    <input type="password" class="form-control" placeholder="Пароль" name="password" required>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" name="reg" class="btn btn-default btn-sm">Регистрация</button>
                </div>
            </div>
    </form>
</div>
</body>
</html>