<div class="form">
    <form class="form-horizontal" action method="POST">
        <div class="form-group">
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Фамилия</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Фамилия" name="f" value="<? echo $accountInfo['fam']?>" required>
                </div>
            </div>

            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Имя</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Имя" name="n" value="<? echo $accountInfo['name']?>" required>
                </div>
            </div>

            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Отчество</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Отчество" name="o" value="<? echo $accountInfo['otch']?>" required>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" name="updateUser" class="btn btn-default btn-sm">Сохранить</button>
                </div>
            </div>
    </form>
    <form class="form-horizontal" action method="POST">
        <div class="form-group">
            <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">Новый пароль</label>
                <div class="col-sm-10">
                    <input type="password" class="form-control" placeholder="Пароль" name="newPassword" required>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" name="updatePassword" class="btn btn-default btn-sm">Сохранить</button>
                </div>
            </div>
    </form>
</div>
